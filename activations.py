import numpy as np

def sigmoid(n):

    n = np.clip(n,-500,500)

    return 1/(1+np.exp(-n))

def sigmoid_prime(n):

    n = np.clip(n,-500,500)

    return sigmoid(n)*(1-sigmoid(n))

def Relu(n):

    r = np.clip(n,0,None)

    return r

def Relu_prime(n):

    r = np.zeros((len(n),len(n[0])),dtype=float)

    for i in range(len(n)):

        for w in range(len(n[0])):

            x = n[i][w]

            if(x > 0):

                r[i][w] = 1

            else:

                r[i][w] = 0

    return r

def row_softmax(row):

    #helps avoiding np.exp from exploding by making it going to zero instead of infinity
    distortion = -np.amax(row)

    row_sum = np.sum(np.exp(row+distortion))

    return np.exp(row+distortion)/row_sum

def softmax(n):

    return np.apply_along_axis(row_softmax,1,n)

def softmax_prime(n):

    return softmax(n)*(1-softmax(n))
