import random
def shuffle_arrays(a,b):

    sa = []
    sb = []
    i = [x for x in range(len(a))]
    random.shuffle(i)

    sa = [a[i[x]] for x in range(len(a))]
    sb = [b[i[x]] for x in range(len(a))]

    return sa,sb
