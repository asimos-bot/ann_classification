class Arch(object):

	def __init__(self, specs_arr, types_arr):
	
		self.specs = specs_arr
		self.types = types_arr
		
if( __name__ == "__main__" ):

	types = ["conv","pool","relu","ann"]

	specs = [[300,300], [3,3], None, [784,200,10]]

	layers = Layers(specs,types)
