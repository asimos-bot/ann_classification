#!/usr/bin/env python3
from arch import Arch
from simple_layer import Simple_Layer
from activations import sigmoid_prime,sigmoid,Relu,Relu_prime,softmax,softmax_prime
import time, random, helper
import numpy as np

class NeuralNetwork(object):

    def __init__(self,arch_obj,filename=None):

        self.filename = filename

        self.specs = arch_obj.specs
	
        self.types = arch_obj.types

        self.n = len(self.specs) - 1

        self.w = []

        self.b = []

        for i in range(self.n):

            low = -pow(self.specs[i],-0.5)

            high = -low

            rows = self.specs[i]

            columns = self.specs[i+1]

            self.w.append(np.random.uniform(low,high,(rows,columns)))
            self.b.append(random.random())

    def forward(self,x):
        #Reset/Initialize all activation and activity
        self.z = [None] * self.n
        self.a = [None] * self.n

        #second layer activity and activation
        self.z[0] = np.dot(x,self.w[0]) + self.b[0]
        self.a[0] = Relu(self.z[0]) if self.types[0] == "sigmoid" else Relu(self.z[0])

        #hidden layers(except first) activity and activation
        for i in range(1,self.n-1):

            self.z[i] = np.dot(self.a[i-1],self.w[i]) + self.b[i]
            self.a[i] = Relu(self.z[i]) if self.types[i] == "relu" else sigmoid(self.z[i])

        #output layer activation and activity
        self.z[-1] = np.dot(self.a[-2],self.w[-1]) + self.b[-1]
        self.a[-1] = sigmoid(self.z[-1]) if self.types[-1] == "sigmoid" else softmax(self.z[-1])

        return self.a[-1]

    def backprop(self,x,y,Lambda):

        yHat = self.forward(x)

        delta = [None] * self.n
        dJd = [None] * self.n

        #output layer
        
        output_activation_prime = sigmoid_prime(self.z[-1]) if self.types[-1] == "sigmoid" else softmax_prime(self.z[-1])
        
        delta[-1] = np.multiply((yHat - y), output_activation_prime )
        dJd[-1] = np.dot(self.a[-2].T,delta[-1]) + Lambda * self.w[-1]

        for i in range(2,self.n):

            #hidden layers(except first hidden layer)
            hidden_layer_activation_prime = Relu_prime(self.z[-i]) if self.types[-i] == "relu" else sigmoid_prime(self.z[-i])
            
            delta[-i] = np.dot(delta[+1-i],self.w[+1-i].T) * hidden_layer_activation_prime
            dJd[-i] = np.dot(self.a[-i-1].T,delta[-i]) + Lambda * self.w[-i]

        #second layer
        input_activation_prime = sigmoid_prime(self.z[0]) if self.types[0] == "sigmoid" else softmax_prime(self.z[0])
        delta[0] = np.dot(delta[1],self.w[1].T) * input_activation_prime
        dJd[0] = np.dot(x.T,delta[0]) + Lambda * self.w[0]

        for i in range(self.n):

            dJd.append(np.mean(delta[i]))

        return dJd

    def train(self, x, y, learning_rate, epochs, Lambda=0, momentum=0, quiet=False):

        if(not quiet):

            start = time.clock()

        iteration = 0

        derivatives= self.backprop(x,y,Lambda)

        while(iteration<=epochs):

            for i in range(self.n):

                self.w[i] -= derivatives[i]*learning_rate + self.w[i]*momentum
                self.b[i] -= derivatives[i+self.n]*learning_rate + self.b[i]*momentum

            derivatives= self.backprop(x,y,Lambda)
            iteration += 1

            if(not quiet):

                print("{}/{}".format(iteration,epochs))

        if(not quiet):

            print("complete\n")

            end = time.clock()

            time_training = end - start

            print("Time:" + str(time_training))

            if(self.filename != None):

                np.savez(self.filename,self.w,self.b)

    def train_mini_batch(self, x, y, learning_rate, epochs, Lambda=0, mini_batch_size=32, momentum=0, quiet=False):

        if(not quiet): start = time.clock()

        iteration = 0

        rest = len(x) % mini_batch_size

        dx = np.delete(x,[i for i in range(rest)],0)
        dy = np.delete(y,[i for i in range(rest)],0)

        n_of_mini_batches = int(len(dx)/mini_batch_size)

        while(iteration < epochs):

            i = random.randrange(0,n_of_mini_batches)

            rx = []
            ry = []

            for w in range(mini_batch_size):

                index = i*mini_batch_size+w

                rx.append(dx[index])
                ry.append(dy[index])

            rx,ry = helper.shuffle_arrays(rx,ry)

            rx = np.array((rx),dtype=float)
            ry = np.array((ry),dtype=float)

            self.train(rx,ry,learning_rate,1,Lambda,momentum,True)

            if(not quiet): 
            	print("{}/{}".format(iteration,epochs))
            
            iteration += 1

        if(not quiet):

            print("complete")
        
            end = time.clock()
        
            time_training = end- start
        
            print("Time:" + str(time_training))

        if(self.filename != None):

            np.savez(self.filename,self.w,self.b)

    def load(self,filename):

        arrays = np.load(filename, allow_pickle=True)
        self.w = arrays['arr_0']
        self.b = arrays['arr_1']

if(__name__ == "__main__"):

    import collector

    seed = 1
    #np.random.seed(seed)
    #random.seed(seed)

    train,valid,test = collector.load_mnist('data','mnist.pkl.gz')

    train_x,train_y = collector.unzip(train)

    train_x = collector.np.reshape(train_x,(len(train_x),len(train_x[0])))
    train_y = collector.np.reshape(train_y,(len(train_y),len(train_y[0])))

    #Neural_Network = layers,filename=None

    arch = Arch([784,200,10],["sigmoid","relu","sigmoid"])

    nn = NeuralNetwork(arch,"ann_classification.npz")

    #train: (x,y,learning_rate,epochs,Lambda=0,momentum=0,show=True)

    #train_mini_batch: (x,y,learning_rate,epochs,Lambda=0,mini_batch_size=32,momentum=0)

    nn.load("ann_classification.npz")

    nn.train_mini_batch(train_x,train_y,0.01,50,0,100,0)

    def test_data(data):

        test_x,test_y = collector.unzip(data)

        test_x = np.reshape(test_x,(len(test_x),len(test_x[0])))
        print(test_x)
        r = nn.forward(test_x)

        right = 0

        for i in range(len(r)):

            if(np.argmax(r[i])==test_y[i]):

                right += 1

        print("accuracy:{}%".format(100*right/len(test_y)))

    test_data(test)
    #test_data(valid)
