# ANN Classification

This project is a implementation of an Artificial Neural Network using numpy.

# Motivation

The project was made with educational purposes in mind. I figure the best
way to understand a ANN was to code one myself!

# Installation

The only requirements are `numpy` and `pickle`

# Usage

Each object is represented by an 1D numpy array inside a numpy
matrix.

## Arch Class

To create a neural network you first need to know what will be its
architecture. That is where the `Arch` object comes in.

`Arch` takes in two arguments:

* a list of the number of nodes in each layer

* a list of the activation functions used in each layer ("sigmoid", "softmax" or "relu")

### Example

In `ann_classification.py` we have:

```
arch = Arch([784,200,10],["sigmoid","relu","sigmoid"])
```

Which means the first layer have 784 nodes, the second 200 and the
third and last have 10.

The first layer and layers use sigmoid as activation function and the second
layer uses relu.

## NeuralNetwork Class

To create a Neural Network class, pass the Architecture being used and optionally
a filepath to where the weights will be saved to when the training is done.

### Example

In `ann_classification.py` we have:

```
nn = NeuralNetwork(arch,"ann_classification.npz")
```

### Training

There are 2 training function:

* `train` - Batch learning

* `train_mini_batch` - Mini batch learning

#### Train (Batch)

The function takes in the data (`x`), its labels (`y`), some hyperparameters
used in training the option to don't output nothing:

```
train(x, y, learning_rate, epochs, Lambda=0, momentum=0, quiet=False)
```

If `quiet` is `False`, the progress will be printed at each epoch and also
the total training time.

#### Train (Mini Batch)

Really similar to `train`, but also takes the batch size as argument:

```
train_mini_batch(self, x, y, learning_rate, epochs, Lambda=0, mini_batch_size=32, momentum=0, quiet=False)
```

Every epoch a random sample from the entire dataset is used for training.

### Loading

The network saves the weights automatically when the training is done,
and to load them back again, we use the `load` function:

```
nn.load("ann_classification_weights.npz")
```

Now you are free to train or predict based on previous training sessions!

### Predicting

When you want your network to make a prediction, just use the `forward`
function to perform a forward propagation (and therefore a prediction)
based on a given numpy matrix:

```
result = nn.forward(x)
```

## Quick Go

If you want to see the network running for a quick test, just run the
`ann_classification.py` script. It will perform a training session with
the [MNIST](http://yann.lecun.com/exdb/mnist/) dataset, also
saving/loading everything to the `ann_classification.npz` file.
