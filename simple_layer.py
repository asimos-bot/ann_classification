import numpy as np
import random
from activations import sigmoid_prime,sigmoid,Relu,Relu_prime,softmax,softmax_prime

class Simple_Layer(object):

	def __init__(self, n_input, n_output ,func):

		#saves the name of the activation function being used
		self.activation = func

		#save direct references to the actual functions            	
		if( func == "relu" ):
			self.forward = Relu
			self.backprop = Relu_prime
		elif( func == "softmax" ):
			self.forward_func = softmax
			self.backprop_func = softmax_prime
		else:
			self.forward = sigmoid
			self.backprop = sigmoid_prime
		
		#initalize weights
		low = -pow(n_input,-0.5)

		high = -low
            	
		self.w = np.random.uniform(low,high,(rows,columns))
            	#initialize bias
		self.b = random.random()
            	
	def forward(self, x):
		
		return self.forward_func(x)
		
	def backprop(self, x, y, learning_rate, next_layer_derivative=None, l=0):
	
		y_hat = self.forward(x)
		
		
		
